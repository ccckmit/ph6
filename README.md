# Ph6 -- 把物理學寫為程式的結果

## 安裝

```
$ npm i ph6
```

## 使用

檔案： hAtomEx.js

```
var ph6 = require('ph6')
var H = ph6.H, E = ph6.Electron
console.log('氫原子半徑=', H.r)

var gHE = ph6.gForce(H.m, E.m, H.r)
var eHE = ph6.eForce(H.e, E.e, H.r)
console.log('氫原子核對電子的重力=', gHE)
console.log('氫原子核對電子的庫倫電力=', eHE)

```

執行結果

```
$ node hAtomEx
氫原子半徑= 5.3e-11
氫原子核對電子的重力= 3.594843625130652e-47
氫原子核對電子的庫倫電力= -8.211350754788181e-8
```

其他範例請參考 [example](example) 資料夾！


