/* eslint-disable no-undef */
var ph6 = require('../ph6')
var A=require('../lib/assert')

describe('Law', function () {
  describe('Hook', function () {
    it('hookForce({k:3,dL:5})=15', function () {
      A.ok(ph6.hookForce({k:3, dL:5})===15)
    })
    it('gForce({m1:1, m2:1, r:1})=G', function () {
      A.ok(ph6.gForce({m1:1, m2:1, r:1})===ph6.G)
    })
    it('地球對表面物體的引力場強度 = 9.83', function () { // 地球對表面物體的引力場強度= 9.83
      let E = ph6.Earth
      A.near(ph6.gForce({m1:E.m, m2:1, r:E.r}), 9.83)
    })
    it('eForce({e1:1, e2:1, r:1})=e', function () {
      A.ok(ph6.eForce({e1:1, e2:1, r:1})===-1*ph6.Ke)
    })
  })
})