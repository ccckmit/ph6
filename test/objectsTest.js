/* eslint-disable no-undef */
var ph6 = require('../ph6')
var A=require('../lib/assert')

var W = new ph6.World()
var o2 = W.addObj('o2', {m: 1, at:[0, 15, 0]})
var o1 = W.addObj('o1', {m: 2, at:[0, 0, 0], fixed:true})
var h12= W.addHook('h12', {m: 0, o1:o1, o2:o2, k:3, L0: 10})

describe('Objects', function () {
  describe('Hook', function () {
    it('h12.force(o1)', function () {
      A.near(h12.force(o1)[1], 15)
    })
    it('force12 == -force21', function () {
      A.near(h12.force(o1)[1], h12.force(o2)[1]*-1)
    })
  })
})