var j6 = require('j6')
var P = module.exports = require('./physics')

P.distance = function (p1, p2) {
  return p1.sub(p2).norm()
}

P.unitVector = function (p1, p2) {
  let d12 = p1.sub(p2)
  return d12.mul(1/d12.norm())
}

// 物體 
P.Object = class Obj {
  constructor(params) {
    Object.assign(this, params)
    this.e = this.e || 0
    this.m = this.m || 0
    this.at = this.at || [0,0,0]
    this.v = this.v || [0,0,0]
    this.a = this.a || [0,0,0]
  }
  gField(p) { // 本物體所造成的引力場 (對任一點 p 的力)
    let m=this.m, d12 = this.at.sub(p), r = d12.norm()
    let F = P.gForce({m1:m, m2:1, r:r}) // 強度
    return d12.mul(1/r).mul(F) // 引力
  }
  gVector(o2) {
    return this.gField(o2.at).mul(o2.m)
  }
  eField(p) { // 本物體所造成的電場 (對任一點 p 的力)
    let e = this.e, d12 = this.at.sub(p), r = d12.norm()
    let F = P.eForce({e1:e, e2:1, r:r}) // 強度
    return d12.mul(1/r).mul(F) // 電作用力
  }
  eVector(o2) {
    return this.eField(o2.at).mul(o2.e)
  }
}

// 彈簧
P.Hook = class Hook extends P.Object {
  constructor(params) {
    super(params)
  }
  force (o) {
    let d12 = this.o1.at.sub(this.o2.at)
    let L = d12.norm()
    let v12 = d12.mul(1/L)
    if (o.name === this.o1.name) v12 = v12.mul(-1)
    let dL = L - this.L0
    let F = P.hookForce({k:this.k, dL:dL})
    let force = v12.mul(F)
    return force
  }
}

P.forceTotal = function (type, o1, objs) {
  let force = [0,0,0]
  let c1 = {'g':o1.m, 'e': o1.e}[type]
  for (let i in objs) {
    if (objs[i] !== o1) {
      let field = objs[i][type+'Field'](o1.at)
      // console.log('field=%j force=%j c1=%j', field, force, c1)
      force = force.add(field.mul(c1))
    }
  }
  return force
}

/*
// 彈簧
P.EHook = class Hook extends P.Object {
  constructor(params) {
    super(params)
  }
}

// 磁鐵
P.Magnetic = class Magnetic extends P.Object {
  constructor(params) {
    super(params)
  }
}

// 螺線管 : 電磁鐵 B=\mu n i
P.Solenoid = class Magnetic extends P.Object {
  constructor(params) {
    super(params)
  }
}

P.distance = function (at1, at2) {
  let d12 = at1.sub(at2)
  return d12.norm()
}

P.gStrength = function (m1, r) {
  return P.G * m1 / (r*r)
}

// 兩質點間的重力 f(m1, m2) = (G*m1*m2)/(r^2)
P.gForce = function (m1, m2, r) {
  return P.gStrength(m1, r) * m2
}

P.gVector = function (o1, o2) {
  let d12 = o1.at.sub(o2.at)
  let r = d12.norm()
  let v12 = d12.mul(1/r)
  let force = v12.mul(P.gForce(o1.m, o2.m, r))
  return force
}

P.fTotal = function (type, o1, objs) {
  let force = [0,0,0]
  for (let i in objs) {
    if (objs[i] !== o1)
      force = force.add(P[type+'Vector'](objs[i], o1))
  }
  return force
}

P.eStrength = function (e1, r) {
  return P.Ke * e1 / (r*r)
}

// 兩質點間的電吸引力 f(e1, e2) = (Ke*e1*e2)/(r^2)
P.eForce = function (e1, e2, r) {
  return P.eStrength(e1, r) * e2
}

P.eVector = function (o1, o2) {
  let d12 = o1.at.sub(o2.at)
  let r = d12.norm()
  let v12 = d12.mul(1/r)
  let force = v12.mul(P.eForce(o1.e, o2.e, r)).mul(-1) // 和重力的情況不同，同性相斥、異性相吸。
  return force
}

// 虎克定律: f = k dL
P.hookForce = function (k, dL) {
  return k * dL
}

P.hookVector = function (o, hook) {
  // console.log('hook=', hook)
  let d12 = hook.o1.at.sub(hook.o2.at)
  let L = d12.norm()
  let v12 = d12.mul(1/L)
  if (o.name === hook.o1.name) v12 = v12.mul(-1)
  let dL = L - hook.L0
  let force = v12.mul(P.hookForce(hook.k, dL))
  console.log('hookVector=%j', force)
  return force
}

// 電磁縮簧: f = - I k dL
P.eHookForce = function (I, k, dL) {
  return -I * k * dL
}

*/
