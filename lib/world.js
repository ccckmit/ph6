var P = require('./objects')

var World = module.exports = class {
  constructor() {
    this.oMap = {}
    this.hookMap = {}
  }
  addObj(name, param) {
    let o = new P.Object(param)
    o.name = name
    this.oMap[name] = o
    return o
  }
  addHook(name, param) {
    let hook = new P.Hook(param)
    this.hookMap[name] = hook
    return hook
  }
  fVector(o) {
    let gForce = P.forceTotal('g', o, this.oMap)
    let eForce = P.forceTotal('e', o, this.oMap)
    let force = eForce.add(gForce)
    // let force = [0,0,0]
    for (let k in this.hookMap) {
      let hook = this.hookMap[k]
      // console.log('hook=', hook)
      let hForce = hook.force(o)
      // console.log('  hForce:', hForce)
      force = force.add(hForce)
    }
    return force
  }
  timePass(t, dt) {
    let tMap = {} 
    for (let k in this.oMap) {
      let o = this.oMap[k]
      if (!o.fixed && o.at != null) {
        let force = this.fVector(o)
        let a = o.a = force.mul(1/o.m)
        let v = o.v, v2 = o.v.add(a.mul(dt))
        let ot = {} // 用 ot = {...o} 會 深度 clone，這樣 hook(o1,o2) 的連結會斷掉，所以不行
        ot.at = o.at.add(v.mul(dt))
        ot.v = v2
        tMap[o.name] = ot
      }
      // tMap[o.name] = ot
    }
    for (let k in this.oMap) {
      let o = this.oMap[k]
      let ot = tMap[k]
      if (ot != null) {
        o.at = ot.at
        o.v = ot.v
      }
    }
  }
  run(param) {
    param = param || {}
    let T = param.T || 10000
    let dt = param.dt || 0.01
    let printStep = param.printStep || 1
    let i = 0
    for (let t = 0; t < T; t+= dt) {
      if (i % printStep===0) console.log('%d: %j\n', t.toFixed(2), this.oMap)
      this.timePass(t, dt)
      i++
    }
  }
}
