var A = module.exports = require('assert')

A.near = function (a, b, range=0.01) {
  return A.ok(Math.abs(a-b) < range, 'near('+a+','+b+') in '+range)
}