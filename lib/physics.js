var A = require('./assert')

var P = module.exports = {
  x: 0, y:1, z:2,   // 坐標軸代號
  // 基本物理量
  kg: 1, // 公斤
  s:  1, // 秒
  m:  1, // 公尺
  K:  1, // 溫度
  A:  1, // 安培
  mol:1, // 莫耳
  cd: 1, // 燭光
  // 衍生單位
  g:  1e-3,    // 公克
  cm: 1e-2,    // 釐米 (公分)
  mm: 1e-3,    // 毫米
  um: 1e-6,    // 微米
  nm: 1e-9,    // 奈米
  pm: 1e-12,   // 皮米
  fm: 1e-15,   // 飛米
/* 組合單位:
  參考 (力) https://zh.wikipedia.org/wiki/%E5%9B%BD%E9%99%85%E5%8D%95%E4%BD%8D%E5%88%B6
  赫茲   Hz= 1/s      // 每秒幾次
  牛頓   N = kg m/s^2 // f = ma
  焦耳   J = N m      // 能量單位 = 牛頓*米
  瓦特   W = J/s      // 功率單位 = 1 焦耳每秒
  庫倫   C = A*s      // 電量單位
  伏特   V = W/A      // 在載有1A恆定電流的導線上，當兩點之間導線上的功率耗散為1W(1W=1 J/S)時，這兩點之間的電位差
  歐姆   Ω = V/A      // 兩點間給予 1 伏特電壓，在導體上可產生 1 安培電流時，此時兩點間的電阻是 1 歐姆
  韋伯   Wb = V s     // 磁通量, 平均每秒一個韋伯的變化可以產生一伏特的電動勢
  特斯拉 T = Wb/m^2   // 磁感應強度單位，每平方米的韋伯數
  亨利   H = Wb/A     // 電感強度，每安培可造成的磁通量變化率
  法拉   F = C/V      // 每伏特電壓可容納一庫倫的電容量，稱為法拉
*/
  minute:60,   // 分鐘
  hour:60*60,  // 小時
  day:60*60*24,// 日
  year:60*60*24*365, // 年
  km:1000,     // 公里
  // 物理常數
  u:  1.661e-27,    // kg 原子質量單位
  e:  1.602e-19,    // C 電子帶電量
  Me: 9.109e-31,    // kg 電子質量
  G : 6.67408e-11,  // N m^2/kg^2 重力常數
  C : 2.99792458e8, // m/s 光速
  Ke: 8.98755e9,    // N m^2/C^2  庫倫常數, 這裡的單位 C 是庫倫，非光速
  Kb: 1.381e-23,    // J/K 波茲曼常數
  Na: 6.022e23,     // 1/mol 亞佛加厥數
  R:  8.314,        // J/K mol 氣體常數
  h:  6.626e-34,    // J s 普朗克常數
  Earth: { // 地球
    r: 6.37e6,  // m 半徑
    m: 5.98e24, // kg 質量 
  },
  Moon: {
    r: 1.74e6,  // m 半徑
    m: 7.36e22, // kg 質量
  },
  Sun: {
    r: 6.96e8,  // m 半徑
    m: 1.99e30, // kg 質量
  },
}

P.Mu0 = 4*Math.PI*10e-7,  // 真空磁導率
P.Ep0 = 1.0/(P.Mu0*P.C0*P.C0), // =8.854e-12  // 真空電容率
P.dist = {
  Sun_Earth:  149597870700, // 1.49e11 太陽到地球的距離 149,597,870,700
  Earth_Moon: 3.84e8,  // 地球到月球的距離
}

P.AU = P.dist.Sun_Earth // 日地距 = 一天文單位 AU

// 光子
P.Photon = {
  speed: P.C, // 光速
  // 靜止質量為 0, 能量 hv, 移動質量 hv/c^2
}
// 電子
P.Electron = {
  e: -1 * P.e,  // 電子電量
  m: P.Me, // 電子質量
}
// 質子 -- https://en.wikipedia.org/wiki/Proton
P.Proton = {
  e: P.e,
  m: 1.6726e-27,
  r: 0.84*P.fm,
}
// 原子半徑 -- https://en.wikipedia.org/wiki/Atomic_radius
P.H = {
  e: P.e,
  m: P.u,
  r: 25*P.pm,
}
// 水
P.Water = {
  density: 1000, // kg/m^3
  specificHeat: 4186, // J/kg K
}

// 聲波
P.Sound = {
  speed: 331.5, // m/s , 0 度 c 時
}

// 大氣
P.Atmosphere = {
  pressure: 1.013e5, // Pa=N/m^2=kg·m/(s^2m^2)
}

P.hookForce = function ({k, dL}) {
  A.ok(k != null && dL != null, 'hookForce: argument (k, dL) not defined!')
  return k * dL
}

P.gForce = function ({m1, m2, r}) {
  A.ok(m1 != null && m2 != null && r != null, 'gForce: argument (m1, m2, r) not defined!')
  return P.G * m1 * m2 / (r*r)
}

P.eForce = function ({e1, e2, r}) {
  A.ok(e1 != null && e2 != null && r != null, 'hookForce: argument (e1, e2, r) not defined!')
  return P.Ke * e1 * e2 / (r*r) * -1
}
