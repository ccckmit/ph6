var P = require('./physics')

var R = module.exports = {}

// 史瓦西半徑:形成黑洞的半徑 -- https://en.wikipedia.org/wiki/Schwarzschild_radius
R.schwarzschildRadius = R.blackHoleRadius = function (m) {
  return 2*P.G*m/(P.C*P.C)
}
