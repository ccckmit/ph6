var ph6 = require('../ph6')

var R = ph6.Relativity

console.log('太陽半徑縮小到多小會變成黑洞？')
console.log('  blackHoleRadius(Sun)=%dm', R.blackHoleRadius(ph6.Sun.m))
console.log('地球半徑縮小到多小會變成黑洞？')
console.log('  blackHoleRadius(Earth)=%dm', R.blackHoleRadius(ph6.Earth.m))
console.log('一公斤的物體半徑縮小到多小會變成黑洞？')
console.log('  blackHoleRadius(1kg)=%dm', R.blackHoleRadius(1))
console.log('原子核半徑縮小到多小會變成黑洞？')
console.log('  blackHoleRadius(Atom)=%dm', R.blackHoleRadius(ph6.H.m))
