var ph6 = require('../ph6')

var W = new ph6.World()

var o1 = W.addObj('o1', {m: ph6.u, e: 1*ph6.e, at:[0, 0, 0]})
var e1 = W.addObj('e1', {m: ph6.Me, e: -1*ph6.e, at:[53*ph6.pm,0,0]})
var e2 = W.addObj('e2', {m: ph6.Me, e: -1*ph6.e, at:[0,53*ph6.pm,0]})

console.log('原子電子間的重力:')
console.log('gVector(o1, e1)=', o1.gVector(e1))
console.log('原子電子間的庫倫電力:')
console.log('eVector(o1,e1)=', o1.eVector(e1))
console.log('eVector(e1,o1)=', o1.eVector(e1))
console.log('eVector(e2,o1)=', e2.eVector(o1))
console.log('原子核受到兩個電子的拉力:')
console.log('fVector(o1)=', W.fVector(o1))
