var ph6 = require('../ph6')

var W = new ph6.World()

var m = ph6.Earth.m, r = ph6.Earth.r

var o1 = W.addObj('o1', {m: m, r: r, at:[-10*r, 0, 0], v:[0, 0, 0]})
var o2 = W.addObj('o2', {m: m, r: r, at:[ 10*r, 0, 0], v:[0, 0, 0]})

// 這個程式有問題，因為兩球 o1, o2 重疊時，引力會無限大，而最後一步接近重疊時力量會太大，導致衝很快，
// 於是原本應該要週期性震盪的，結果沒有 (因為衝很快一下就衝過頭，沒有到對稱的另一邊去再減速)
W.run(1000000, 10)
