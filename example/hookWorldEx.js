var ph6 = require('../ph6')

var k=3, dL=5
console.log('hook(%d,%d)=%d', k, dL, ph6.hookForce({k:k, dL:dL}))

var W = new ph6.World()

var o2 = W.addObj('o2', {m: 1, at:[0, 15, 0]})
var o1 = W.addObj('o1', {m: 2, at:[0, 0, 0], fixed:true})
var h12= W.addHook('h12', {m: 0, o1:o1, o2:o2, k:k, L0: 10})
/*
console.log('h12.force(o1)=', h12.force(o1))
console.log('h12.force(o2)=', h12.force(o2))

o2.at[1] = 5
console.log('h12.force(o1)=', h12.force(o1))
console.log('h12.force(o2)=', h12.force(o2))
*/
W.run({T: 5, dt: 0.01})

