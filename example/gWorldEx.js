var ph6 = require('../ph6')

var W = new ph6.World()

var o1 = W.addObj('o1', {m: 1e5, at:[0, 0, 0], v:[0, 0, 0]})
var o2 = W.addObj('o2', {m: 100e5, at:[10,0,0], v:[0, 0, 0]})
var o3 = W.addObj('o3', {m: 100e5, at:[0,10,0], v:[0, 0, 0]})

console.log('gVector(o1,o2)=', o1.gVector(o2))
console.log('gVector(o2,o1)=', o2.gVector(o1))
console.log('gVector(o3,o1)=', o3.gVector(o1))
console.log('兩顆一千萬公斤的球對一顆十萬公斤的球之合成引力')
console.log('fVector(o1)=', W.fVector(o1))
