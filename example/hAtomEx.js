var ph6 = require('../ph6')
var H = ph6.H, E = ph6.Electron
console.log('氫原子半徑=', H.r)

var gHE = ph6.gForce({m1:H.m, m2:E.m, r:H.r})
var eHE = ph6.eForce({e1:H.e, e2:E.e, r:H.r})
console.log('氫原子核對電子的重力=', gHE)
console.log('氫原子核對電子的庫倫電力=', eHE)
