var ph6 = require('../ph6')

var W = new ph6.World()

var Sun = ph6.Sun, Moon = ph6.Moon

var sun1 = W.addObj('sun1', {m: Sun.m, at:[-Sun.r, 0, 0], v:[0, 0, 0], fixed:true})
var sun2 = W.addObj('sun2', {m: Sun.m, at:[ Sun.r, 0, 0], v:[0, 0, 0], fixed:true})
var moon = W.addObj('moon', {m: Moon.m, at:[ 0, Sun.r, 0], v:[0, 0, 0], fixed:false})
// var sun = W.addObj('sun', {m: Sun.m, at:[ 0, Sun.r, 0], v:[0, 0, 0], fixed:false})

W.run({T: 20000, dt: 1, printStep:100})
