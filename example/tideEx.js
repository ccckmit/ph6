var ph6 = require('../ph6')

console.log('月地距/地球半徑=', ph6.dist.Earth_Moon/ph6.Earth.r)
console.log('日地距/地球半徑=', ph6.dist.Sun_Earth/ph6.Earth.r)

var g = ph6.gStrength(ph6.Earth.m, ph6.Earth.r)
var gSE = ph6.gStrength(ph6.Sun.m, ph6.dist.Sun_Earth)
var gME = ph6.gStrength(ph6.Moon.m, ph6.dist.Earth_Moon)
console.log('地球對表面物體的引力場強度=', g)
console.log('太陽對地球物體的引力場強度=', gSE)
console.log('月球對地球物體的引力場強度=', gME)

console.log('月球對地球背面的引力差=', gME-ph6.gStrength(ph6.Moon.m, ph6.dist.Earth_Moon+ph6.Earth.r))
console.log('太陽對地球背面的引力差=', gSE-ph6.gStrength(ph6.Sun.m, ph6.dist.Sun_Earth+ph6.Earth.r))

/*
console.log('太陽對地球物體的引力強度=', ph6.g(ph6.Sun.m, ph6.Earth.m, ph6.dist.Sun_Earth)/ph6.Earth.m)
console.log('月球對地球物體的引力強度=', ph6.g(ph6.Moon.m, ph6.Earth.m, ph6.dist.Earth_Moon)/ph6.Earth.m)
*/