# 物理公式

## 單位

基本單位

公斤   kg           // 質量
秒     s            // 時間
公尺   m            // 長度
克耳文 K            // 絕對溫度
安培   A            // 電流量
莫耳   mol          // 原子數量
燭光   cd           // 光度

導出單位

赫茲   Hz= 1/s      // 每秒幾次
牛頓   N = kg m/s^2 // f = ma
焦耳   J = N m      // 能量單位 = 牛頓*米
瓦特   W = J/s      // 功率單位 = 1 焦耳每秒
庫倫   C = A*s      // 電量單位
伏特   V = W/A      // 在載有1A恆定電流的導線上，當兩點之間導線上的功率耗散為1W(1W=1 J/S)時，這兩點之間的電位差
歐姆   Ω = V/A      // 兩點間給予 1 伏特電壓，在導體上可產生 1 安培電流時，此時兩點間的電阻是 1 歐姆
韋伯   Wb = V s     // 磁通量, 平均每秒一個韋伯的變化可以產生一伏特的電動勢
特斯拉 T = Wb/m^2   // 磁感應強度單位，每平方米的韋伯數 (B場, B = mu' H)
亨利   H = Wb/A     // 電感強度，每安培可造成的磁通量變化率 (磁場強度 H 場, H = B/mu'), 參考: https://zh.wikipedia.org/wiki/%E7%A3%81%E5%A0%B4
法拉   F = C/V      // 每伏特電壓可容納一庫倫的電容量，稱為法拉

單位常見基準


## 牛頓力學

兩質點間的重力 f(m1, m2) = (G*m1*m2)/(r^2)

第一運動定律: f=0, a=0  // 無力 => 等速
第二運動定律: f=ma
第三運動定律: f=-f'     // 作用力 = 反作用力 

向心力: f=m v^2/r

重力： G = mg

動摩擦力： f=mu N

靜摩擦力 <  動摩擦力

dL = v dt // 距離 = 速度*時間

動能 Ek = 1/2 m v^2

位能 Ep = m g h

彈性位能 Ep = 1/2 k dL^2

功 W = f dL

動量 P = m v

衝量 I = f t

## 機械力學

虎克定律 f = k dL     // k 彈性係數 dL 伸縮距離
力矩 M = fL 

## 幾何學

圓面積: A = pi r^2
球體積: V = 4/3 pi r^3

## 流體力學

浮力 f=d g V          // d 密度 g重力 V體積
密度 d=m/V
壓強 p=F/S
液體壓強 p=dgh

## 波動

v = L f      // L 波長 f 頻率

## 電磁學

電場強度 
  通用     E = f/q  
  點電荷   E = kQ/r^2
  均勻電場 E = U/d

電場力
  通用 f = q E
  庫倫定律 f(q1, q2) = (Ke*q1*q2)/(r^2) // 兩質點間的電吸引力

磁場力 (這沒在定律裏，但是可以推算 http://w3.phys.nthu.edu.tw/~exphy/Download/ex01.pdf)
  分三種 case 計算
  1 目標點距離磁鐵很遠 (可視為很小的磁矩) Mu0 (3 (Mu rv)rv - Mu)/(4Pi r^3)
  2 目標點距離磁鐵不遠，但在磁鐵外部 (Mu0/4Pi)*(f(n) - f(s))   f(x) = (Mu L rv(x)/r^2)
  3 目標點在磁鐵內部 Mu0 M     (其中 M = Mu/V 單位體積內的磁矩，既磁化強度)

電容
  C = Q/V    // 電容 = 電量 / 電壓
  C = Ep A/d // Ep 介質電容率
  E = 1/2 CV^2
  串聯 1/C = 1/C1+1/C2+... 並聯 C=C1+C2+....

 電流
   I = dQ/dt = C dV/dt

 電阻
   歐姆定律 R = V/I
   串聯電阻 R=R1+R2+... 並聯電阻 1/R = 1/R1+1/R2+...

  電功率
    P = VI = I^2 R // 焦耳定律
  電功
    W = Pt

  磁場
    安培定律 \int B dL = mu0 I
    長直導線 B = (mu0 I)/(2 Pi r)
    螺線管 B = mu0 n I      // n 圈數
  磁通量
    通用磁通量         Phi = \int B dA
    均勻磁場通過斜平面  Phi = B A cos(角)
    法拉第定律         感應電動勢 E = - d(Phi)/dt

    冷次定律:感應電動勢的產生是要反對通過他的磁通量變化
      
  發電機
    Phi = BA cos(w t) // w:角速度  => 感應電動勢 E =-N d(Phi)/dt = N A B w sin(wt)

  電感
    E = - d(N Phi)/dt
    
馬克士威方程組
  高斯電場定律     \int E dA = Q/Ep0                     // 電通量總和=內部電荷數 (包合面)
  高斯磁場定律     \int B dA = 0                         // 磁通量總和=0         (任意曲面)
  法拉第定律       \int E dL = - d(Phi)/dt               // 改變的磁場會造成電流  (環線)
  安培馬克士威定律 \int B dL = mu0 (I + Ep d(Phi)/dt)     // 電流+磁通量改變 = 磁場變化  (環線)
加上
  勞倫茲力方程式    F = q(E + v B)
  電荷守恆定律
就可以描述所有電磁現象與元件了！

電磁波速度
  C = 1/sqrt(mu0 ep0)

## 相對論

史瓦西半徑::形成黑洞的半徑 -- https://en.wikipedia.org/wiki/Schwarzschild_radius

  rs = 2*G*m/C^2

## 參考文獻

* http://www.kwongtai.edu.mo/download/resource/physics/high_school_phy_fomulas.pdf
